const { app, BrowserWindow, Menu } = require('electron');
const url = require('url');
const path = require('path');
const Store = require('electron-store');

const store = new Store();

const createWindow = () => {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            webviewTag: true,
            preload: path.join(__dirname, 'preload.js'),
            contextIsolation: true,
            nodeIntegration: true,
            webSecurity: false,
        },
    });

    win.webContents.session.webRequest.onBeforeSendHeaders((details, callback) => {
       callback({ requestHeaders: { Origin: "*", ...details.requestHeaders }});
    });

    win.webContents.session.webRequest.onHeadersReceived((details, callback) => {
       callback({
           responseHeaders: {
               'Access-Control-Allow-Origin': ["*"],
               'Access-Control-Allow-Headers': ["*"],
               ...details.responseHeaders,
           }
       });
    });

    win.loadURL(url.format ({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))
    win.maximize();
    setMainMenu(win);
}

app.whenReady().then(() => {
    createWindow();

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow()
    })
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit();
});

const setMainMenu = (win) => {
    const template = [
        {
            label: 'ЮрКлуб',
            submenu: [
                {
                    label: 'Сменить URL приложения',
                    click() {
                        store.delete('APP_URL');
                        win.reload();
                    }
                }
            ]
        }
    ];
    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
}
