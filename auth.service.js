const ECHO_ROUTE = '/spring/rest/v0/auth/echo';
const ACTIVATE_ROUTE = (code) => `/spring/rest/v0/auth/activate/${code}`;

module.exports = {
    ECHO_ROUTE,
    ACTIVATE_ROUTE,
};